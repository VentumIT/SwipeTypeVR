﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SwipeType
{
    public class GazeRaycaster : MonoBehaviour
    {
        public Image Circle;
        public float LoadingTime;

        private string lastTargetName = string.Empty;

        private void FixedUpdate()
        {
            RaycastHit hit;

            Vector3 fwd = transform.TransformDirection(Vector3.forward);

            if (Physics.Raycast(transform.position, fwd, out hit) && hit.transform.tag == "VRGazeInteractable")
            {
                // Check if we have already gazed over the object.
                if (lastTargetName == hit.transform.name)
                    return;

                // Set the last hit if last targer is empty.
                if (lastTargetName == "")
                    lastTargetName = hit.transform.name;

                // Check if current hit is same with last one;
                if (hit.transform.name != lastTargetName)
                {
                    Circle.fillAmount = 0f;
                    lastTargetName = hit.transform.name;
                    return;
                }

                StartCoroutine(FillCircle(hit.transform));
                return;
            }

            ResetGazer();
        }

        private IEnumerator FillCircle(Transform target)
        {
            // When the circle starts to fill, reset the timer.
            float timer = 0f;
            Circle.fillAmount = 0f;

            while (timer < LoadingTime)
            {
                if (target.name != lastTargetName)
                    yield break;

                timer += Time.deltaTime;
                Circle.fillAmount = timer / LoadingTime;
                yield return null;
            }

            Circle.fillAmount = 1f;

            if (target.GetComponent<Button>())
                target.GetComponent<Button>().onClick.Invoke();

            ResetGazer();
        }

        // Reset the loading circle to initial, and clear last detected target.
        private void ResetGazer()
        {
            if (Circle == null)
            {
                Debug.LogError("Please assign target loading image, (ie. circle image)");
                return;
            }

            Circle.fillAmount = 0f;
            lastTargetName = "";
        }
    }
}