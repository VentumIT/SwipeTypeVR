﻿using UnityEngine;
using UnityEngine.UI;

namespace SwipeType
{
    public class SwipeManager : MonoBehaviour
    {
        public float MinSwipeLength = 30f;

        private Vector2 currentPos;
        private Transform prevKey;
        private Vector2 prevPos;

        private void Update() { DetectSwipe(); }

        private void DetectSwipe()
        {
            if (Input.GetMouseButtonDown(0))
            {
                prevPos.x = Input.mousePosition.x;
                prevPos.y = Input.mousePosition.y;
            }

            if (Input.GetMouseButton(0))
            {
                currentPos.x = Input.mousePosition.x;
                currentPos.y = Input.mousePosition.y;
                HandleSwipe();
            }
        }

        private void HandleSwipe()
        {
            if (Vector2.Distance(prevPos, currentPos) < MinSwipeLength)
                return;

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(currentPos);

            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "KeyboardCharacter" && hit.transform != prevKey)
            {
                hit.transform.GetComponent<Button>().onClick.Invoke();
                prevKey = hit.transform;
                prevPos = currentPos;
            }
        }
    }
}