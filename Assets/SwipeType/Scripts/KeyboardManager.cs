﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SwipeType
{
    public class KeyboardManager : MonoBehaviour
    {
        [Header("Essentials")]
        public Transform Characters;

        [Header("UI Elements")]
        public Text InputText;

        [Header("User defined")]
        [Tooltip("If the character is uppercase at the initialization")]
        public bool IsUppercase = false;

        public int MaxInputLength;

        private string Input
        {
            get { return InputText.text; }
            set { InputText.text = value; }
        }

        private readonly Dictionary<GameObject, Text> keysDictionary = new Dictionary<GameObject, Text>();
        private bool capslockFlag;
        private SwipeType swipeType;

        private void Start()
        {
            for (int i = 0; i < Characters.childCount; i++)
            {
                GameObject key = Characters.GetChild(i).gameObject;
                Text text = key.GetComponentInChildren<Text>();
                keysDictionary.Add(key, text);
                key.GetComponent<Button>().onClick.AddListener(() => { GenerateInput(text.text); });
            }

            capslockFlag = IsUppercase;
            CapsLock();

            swipeType = new SimpleSwipeType(File.ReadAllLines("Assets/VRKeyboard/EnglishDictionary.txt"));
        }

        public void Backspace()
        {
            if (Input.Length > 0)
                Input = Input.Remove(Input.Length - 1);
        }

        public void Clear() { Input = ""; }

        public void CapsLock()
        {
            foreach (var pair in keysDictionary)
                pair.Value.text = capslockFlag ? pair.Value.text.ToUpper() : pair.Value.text.ToLower();

            capslockFlag = !capslockFlag;
        }

        public void GenerateInput(string s)
        {
            if (Input.Length > MaxInputLength)
                return;

            Input += s;

            StringBuilder sb = new StringBuilder();
            string[] suggestions = swipeType.GetSuggestion(Input);
            foreach (string str in suggestions)
            {
                sb.Append(str);
                sb.Append(" ");
            }

            Debug.Log(sb.ToString());
        }
    }
}